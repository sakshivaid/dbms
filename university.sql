﻿CREATE TABLE Department (
	Dept_Name   varchar(5) PRIMARY KEY,
	Dept_Room   integer,
	ContactNo   numeric(10)
);

CREATE TABLE Faculty (
	emp_id    varchar(5) PRIMARY KEY,
	emp_name  varchar(20),
	Dept_Name  varchar(5),
	salary     numeric(5),
	Date_of_join  date 
);

CREATE TABLE Students (
	student_id   varchar(10) PRIMARY KEY,
	student_name varchar(20),
	gender       varchar(10),
	department   varchar(5),
	admi_year    integer
);

CREATE TABLE Courses (
	course_code varchar(6) PRIMARY KEY,
	course_title varchar(30),
	credits      integer,
	semester     integer,
	academic_year  integer,
	branch        varchar(10)
);

CREATE TABLE Counselors (
	emp_id  varchar(5),
	student_id   varchar(10)
);

CREATE TABLE teaching (
	emp_id  varchar(5),
	course_id varchar(6),
	semester    integer,
	year 	    integer,
	programme  varchar(10),
	branch        varchar(10)
);

INSERT INTO Department VALUES ( 'CSE', 117, 9898013562 );

INSERT INTO Department VALUES ( 'EEE', 202, 9988736513 );

INSERT INTO Department VALUES ( 'ME', 301, 7389026745 );

INSERT INTO Department VALUES ( 'MscCh', 9, 9567890152);

INSERT INTO Department VALUES ( 'ECE', 101, 7654213562 );

INSERT INTO Department VALUES ( 'MscPH', 312, 8552672891 );

INSERT INTO Department VALUES ( 'BCA', 111, 8897654132 );

INSERT INTO Department VALUES ( 'EIE', 201, 9986517824 );

INSERT INTO Department VALUES ( 'AE', 5, 9892678954 );

INSERT INTO Department VALUES ( 'Bcom', 107, 7768906543 );

INSERT INTO Faculty VALUES ( 'CS101', 'Aswathi', 'CSE', 45000, '2010-11-06');

INSERT INTO Faculty VALUES ( 'EC002', 'Anuja', 'ECE', 47000, '2007-7-16');

INSERT INTO Faculty VALUES ( 'ME081', 'Rajesh', 'ME', 25000, '2014-9-22');

INSERT INTO Faculty VALUES ( 'Mc121', 'Sreelakshmi', 'MscPH', 48000, '2006-12-8');	

INSERT INTO Faculty VALUES ( 'CS111', 'Neetu', 'CSE', 40000, '2010-7-1');

INSERT INTO Faculty VALUES ( 'EE081', 'Sumesh', 'EEE', 45000, '2009-4-13');

INSERT INTO Faculty VALUES ( 'EI221', 'Nikhil', 'EIE', 45000, '2008-11-06');

INSERT INTO Faculty VALUES ( 'CS111', 'Divya', 'CSE', 47000, '2008-11-10');

INSERT INTO Faculty VALUES ( 'EE191', 'Sarath', 'EEE', 45000, '2012-9-7');

INSERT INTO Faculty VALUES ( 'ME061', 'Lata', 'ME', 35000, '2011-11-11');

INSERT INTO Students VALUES ( 'cse13155', 'sreelakshmi','female', 'CSE', 2013); 

INSERT INTO Students VALUES ( 'cse13143', 'pappu','male', 'CSE', 2013);

INSERT INTO Students VALUES ( 'cse13124', 'Nikhil','male', 'CSE', 2013);

INSERT INTO Students VALUES ( 'ece13001', 'aditi','female', 'ECE', 2013);

INSERT INTO Students VALUES ( 'me11101', 'savitri','female', 'ME', 2011);

INSERT INTO Students VALUES ( 'cse12104', 'parvati','female', 'EEE', 2012);

INSERT INTO Students VALUES ( 'mscph1043', 'Ganesh','male', 'MscCh', 2010);

INSERT INTO Students VALUES ( 'cse13123', 'deep','male', 'CSE', 2013);

INSERT INTO Students VALUES ( 'ece11203', 'pallavi','female', 'ECE', 2011);

INSERT INTO Students VALUES ( 'me13133', 'naveen','male', 'ME', 2013);

INSERT INTO Courses VALUES ( 'CSE340', 'DBMS', 4, 5, 2015, 'CSE');

INSERT INTO Courses VALUES ( 'ECE140', 'Signals', 4, 3, 2011, 'ECE');

INSERT INTO Courses VALUES ( 'EEE111', 'Electrical circuits', 4, 6, 2011, 'EEE');

INSERT INTO Courses VALUES ( 'CSE110', 'C++', 3, 2, 2012, 'ME');

INSERT INTO Courses VALUES ( 'Ch210', 'organic Chemistry', 4, 1, 2013, 'MscCh');

INSERT INTO Courses VALUES ( 'PHY100', 'Astrophysics', 2, 2, 2014, 'MscPh');

INSERT INTO Courses VALUES ( 'ECE123', 'Digital Systems', 4, 3, 2015, 'ECE');

INSERT INTO Courses VALUES ( 'ME111', 'Thermodynamics', 3, 3, 2014, 'ME');

INSERT INTO Courses VALUES ( 'CSE440', 'Cryptography', 2, 7, 2015, 'CSE');

INSERT INTO Courses VALUES ( 'CSE100', 'C', 4, 1, 2014, 'CSE');

INSERT INTO Counselors VALUES ('CS101', 'cse13155');

INSERT INTO Counselors VALUES ('EC002', 'ece11203');

INSERT INTO Counselors VALUES ('CS101', 'cse13143');

INSERT INTO Counselors VALUES ('ME061', 'me13133');

INSERT INTO Counselors VALUES ('CS111', 'cse13124');

INSERT INTO Counselors VALUES ('CS111', 'cse12104');

INSERT INTO Counselors VALUES ('Mc121', 'mscph1043');

INSERT INTO Counselors VALUES ('ME061', 'me11101');

INSERT INTO Counselors VALUES ('EC002', 'ece13001');

INSERT INTO teaching VALUES ( 'CS111', 'CSE340', 5, 2015, 'Normal', 'CSE');

INSERT INTO teaching VALUES ( 'CS111', 'CSE340', 5, 2015, 'Redo', 'CSE');

INSERT INTO teaching VALUES ( 'ME081', 'ME111', 3, 2014, 'Normal', 'ME');

INSERT INTO teaching VALUES ( 'EE191', 'EEE111', 6, 2015, 'Normal', 'EEE');

INSERT INTO teaching VALUES ( 'EC002', 'ECE123', 3 ,2015, 'Normal', 'CSE');

INSERT INTO teaching VALUES ( 'EC001', 'ECE123', 3, 2015, 'Redo', 'CSE');

INSERT INTO teaching VALUES ( 'Mc121', 'PHY100', 2, 2014, 'Normal', 'MscPh');

INSERT INTO teaching VALUES ( 'Mc121', 'PHY100', 2, 2014, 'redo', 'MscPh');

INSERT INTO teaching VALUES ( 'EE081','ECE140', 3, 2011, 'Normal', 'ECE');

INSERT INTO teaching VALUES (  'EE081','ECE140', 3, 2011, 'redo', 'ECE');

select dept_name from Department;

select emp_name from faculty where dept_name = 'CSE';

select course_title from courses where credits = 4;

select student_name from students where admi_year = 2013;

select * from teaching;

alter table teaching add constraint teach_pkey primary key (emp_id, course_id, programme);


select * from Counselors;


alter table Counselors add constraint counselor_pkey primary key (emp_id, student_id);

select emp_id from Counselors where student_id = 'cse13155';

select Faculty.emp_name from Counselors , Faculty where student_id = 'cse13155' and Faculty.emp_id = Counselors.emp_id; 

alter table Courses add column exam_slot varchar(10);

select * from Courses;

select max(salary) from Faculty;

select course_code, course_title from Courses where branch = 'ECE';

select Dept_Room as Room_no from Department;

select distinct emp_name from Faculty natural join Counselors;

select emp_name from Faculty where salary > 25000 and salary < 50000;

alter table Faculty add column designation varchar(30);

select * from faculty;

(select course_title from courses where academic_year = 2015 and mod(semester,2) <>0)
union
(select course_title from courses where academic_year = 2013 and mod(semester,2)=0);

select emp_id, course_id from teaching where year = 2012 and mod(semester,2) =0;

alter table department rename Dept_Room to Room_no;

select * from Department;

update Faculty set designation = 'professor' where emp_id = 'CS101';

update Faculty set designation = 'assistant professor' where emp_id = 'EC002';

update Faculty set designation = 'lecturer' where emp_id = 'ME081';

update Faculty set designation = 'teaching assistant' where emp_id = 'Mc121';

update Faculty set designation = 'professor' where emp_id = 'CS111';

update Faculty set designation = 'assistant professor' where emp_id = 'EE081';

update Faculty set designation = 'lecturer' where emp_id = 'EI221';

update Faculty set designation = 'teaching assistant' where emp_id = 'EE191';

update Faculty set designation = 'assistant professor' where emp_id = 'ME061';

select count(designation) from faculty where dept_name = 'ECE';

select count(designation), dept_name from faculty where designation = 'assistant professor' group by  dept_name;

select avg(salary), dept_name from faculty group by dept_name;

select avg(salary), dept_name, designation from faculty group by dept_name, designation;

insert into faculty values ('CS007', 'Ganesh' ,'CSE', null, '2015-3-1', 'lecturer');

insert into faculty values ('EC123', 'Ramesh' ,'ECE', null, '2015-3-1', 'lecturer');

insert into faculty values ('EE127', 'Savitri' ,'EEE', null, '2015-3-1', 'lecturer');

insert into faculty values ('ME452', 'Neelima' ,'ME', null, '2015-3-1', 'lecturer');

insert into faculty values ('EI107', 'Rekha' ,'EIE', null, '2015-3-1', 'lecturer');

select emp_name from faculty where salary is null;

select avg(salary), dept_name from faculty group by dept_name;

select avg(salary), dept_name from faculty group by dept_name having avg(salary) > 5000;

select avg(salary), dept_name from faculty where designation = 'assistant professor' group by dept_name having avg(salary) > 5000;

select emp_name, salary*1.15 as hike from faculty where designation = 'teaching assistant';

select * from faculty;

update faculty set salary = salary*1.15 where designation = 'teaching assistant'and salary < 10000; 

select student_name from students where student_name like '______';

select student_name from students order by student_name asc;

select admi_year from students order by admi_year desc;

select I.emp_name, I.dept_name from faculty as I , faculty as S where I.salary > S.salary and S.dept_name = 'CSE'; 

select emp_name, course_title from faculty, courses, teaching where teaching.emp_id = faculty.emp_id and courses.course_code= teaching.course_id;

select emp_name, course_title from faculty, courses, teaching where teaching.emp_id = faculty.emp_id and courses.course_code= teaching.course_code;

alter table teaching rename course_id to course_code;

select emp_name, course_title from faculty natural join courses natural join teaching;