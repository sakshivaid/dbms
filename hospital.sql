﻿create table dept
(id varchar(20),
 fname varchar(20),
 mname varchar(20),
 lname varchar(20),
 startname varchar(20),
 block varchar(20),
 bno numeric,
 primary key(id));

 create table doctor
 (id varchar(20),
  firstname varchar(20),
  middlename varchar(20),
  lastname varchar(20),
  street varchar(20),
  city varchar(20),
  dateofjoin date,
  specialisation varchar(20),
  deptid varchar(20),
  primary key(id),
  foreign key(deptid) references dept);

  create table patient
  (id varchar(20),
   fname varchar(20),
   mname varchar(20),
   lname varchar(20),
   street varchar(20),
   city varchar(20),
   primary key(id));

   create table docpat
   (docid varchar(20),
    patid varchar(20),
    typeofdisease varchar(20),
    dateofvisit date,
    primary key(docid),
    foreign key(docid) references doctor);

    create table pno
    (docid varchar(20),
     phno numeric,
     primary key(docid),
     foreign key(docid) references doctor);


     insert into dept values('101','e','ram','leela','2011-05-11','main',21);
     insert into dept values('102','f','man','real','2012-06-12','main',22);
     insert into dept values('103','g','nam','lol','2011-05-13','main',23);

     select * from dept;

     insert into doctor values('121','dr','paul','man','gandhinagar','palakkad','2012-05-23','Gynaecology','101');
     insert into doctor values('122','dr','sreekanth','george','venoli','palakkad','2012-05-25','Oncology','102');
     insert into doctor values('123','dr','adith','suresh','kottekad','palakkad','2012-05-28','Orthopaedic','103');
     
     insert into patient values('131','ms','sakthi','vaid','pkd','pgt','55');
     insert into patient values('132','ms','sai','vinodh','pkd','pgt','58');
     insert into patient values('133','ms','pappu','subhash','kty','trs','59');

     insert into docpat values('121','131','fever','2014-06-29');
     insert into docpat values('122','132','dengue','2013-05-28');
     insert into docpat values('123','133','malaria','2013-04-27');

     insert into pno values('121',9447700030);
     insert into pno values('122',8281554863);
     insert into pno values('123',9746478566);

     select id,middlename,specialisation from doctor where specialisation='Gynaecology';

     select count(*),docid from docpat join doctor on docpat.docid=doctor.id 
     where specialisation='Gynaecology' and dateofvisit='2014-06-29'
     group by docid;

     select s.id,s.mname from (docpat join patient on docpat.patid=patient.id)as s join doctor on s.docid=doctor.id  
     where middlename='paul' or middlename='sreekanth';  

     with nop(dept,count) as (select deptid,count(*) from docpat join doctor on docpat.docid=doctor.id group by deptid)
     select avg(count),dept from nop where count<10 group by dept;

     select block,patid,y.mname from ((docpat join doctor on doctor.id=docpat.docid) as s join patient on patient.id=s.patid) as y join dept on dept.id=y.deptid
     where y.mname='sakthi';

     select count(specialisation) from doctor 
     group by id
     having count(*)>1;

     create function assign(patientid varchar(20),department varchar(20),disease varchar(20))
     returns table (docid varchar(20),
                    patid varchar(20),
                    typeofdisease varchar(20),
                    date varchar(20))
     as
     $$
     declare x varchar;
     begin
     x=(select id from doctor
                    
			,
			
     
     



     
		