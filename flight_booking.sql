﻿create table air_ticket (
	ticket_id varchar(10) primary key,
	customer_id varchar(10),
	flight_id varchar(10),
	duration time,
	seat_no varchar(5),
	ticket_type varchar(10),
	booking_status varchar(10),
	price numeric(5),
	book_date date,
	book_time time
);

create  table customer (
	customer_id varchar(10) primary key,
	cust_name varchar(20),
	house_name varchar(10),
	street varchar(20),
	city   varchar(20),
	country varchar(20),
	travel_date date,
	travel_time time
);

create table customer_phone_no (
	customer_id varchar(10),
	phone_no   varchar(14),
	primary key(customer_id, phone_no),
	foreign key(customer_id) references customer
); 

create table airplane (
	airplane_id varchar(10) primary key,
	name varchar(10),
	price varchar(10),
	no_of_seat numeric(3)
);


create table plane_repair (
	airplane_id varchar(10),
	repair_detail varchar(30),
	primary key (airplane_id, repair_detail)
);

create table staff (
	staff_id varchar(10) primary key,
	name varchar(20),
	salary numeric(5),
	position varchar(20)
);


create table flight (
	flight_id varchar(10) primary key,
	country_origin varchar(20),
	country_dest varchar(20),
	dept_date date,
	dept_time time,
	arrival_date date,
	arrival_time time 
);

create table staff_flight (
	staff_id varchar(10),
	flight_id varchar(10),
	primary key(staff_id, flight_id)
);

alter table airplane add flight_id varchar(10);
alter table air_ticket add constraint fkey foreign key(customer_id) references customer; 
alter table airplane add constraint f1key foreign key(flight_id) references flight; 
alter table staff_flight add constraint f2key foreign key (staff_id) references staff;
alter table staff_flight add constraint f3key foreign key (flight_id) references flight;


insert into customer values('123A','deepika','nirmal', 'street1','kollam','india','05-5-2015','08:30');    
insert into customer values('1234A','rachana','sadan', 'street2','hyderabad','india','06-5-2015','09:30'); 
insert into customer values('1234B','pappu','amrita', 'street3','kanpur','india','07-5-2015','10:30'); 
insert into customer values('AR567','sreelakshmi','flat 102', 'street4','calicut','india','08-5-2015','11:30'); 
insert into customer values('BYA12','sakshi','flat 111', 'street5','kollam','gandhinagar','09-5-2015','12:30'); 

insert into air_ticket values('abc1e456', '1234A', 'AF102', '09:30','a5', 'economy','confirmed','12000','2-2-2015','12:20');

select cust_name from air_ticket natural join customer where flight_id = 'AF102';

select flight_id from flight natural join air_ticket where dept_date = '09-15-2015' and booking_status = 'confirmed'group by flight_id having count(ticket_id) >= 60 ;


Create table Members(
	Member_ID  varchar(10),
        Name varchar(20),
        Address varchar(40),
        primary key (Member_ID)
);

 Create table Product_details(
		Prod_name   varchar(35),
                Price       int,
                primary key (Prod_name)
);

Create table Branches(
	Br_name   varchar(20),
	Br_address   varchar(40),
        primary key (Br_name)
);         

  Create table order_details(
	no_of_products  int,
        date_of_purchase  date,
        buyer_name   varchar(30),
        store_branch  varchar(20),
        primary key (no_of_products, date_of_purchase, buyer_name,store_branch)
);                

Create table Employee(
	Emp_ID   int,
        Emp_name  varchar(20),
        primary key (Emp_ID)
);

Create table Members_Branches(
	Member_ID  varchar(10),
        Br_name   varchar(20),
        primary key (Member_ID, Br_name)
);

Create table Order_details_Branches(
	Br_name   varchar(20),
        no_of_products  int,
        date_of_purchase  date,
        buyer_name   varchar(30),
        store_branch  varchar(20),
        primary key (no_of_products, date_of_purchase, buyer_name, store_branch));

 Create table Branches_Employee(
	Emp_ID   int,
        Br_name  varchar(20),
        Designation varchar(20),
        primary key (Emp_ID)
);

alter table Branches add Prod_name varchar(35);

Insert into Members values(101,'Adithya','Mumbai');
Insert into Members values(102,'Avinash','Mumbai');
select * from Members;

Insert into Product_details values('P102',2500);
Insert into Product_details values('P501',25150);
select * from Product_details;

Insert into Branches values('B102','Hyd_1');
Insert into Branches values('B101','Hyd_2');
select * from Branches;

Insert into order_details values(99,'07/29/2015','Adithya','B102');
Insert into order_details values(101,'08/20/2015','Avinash','B101');
select * from Order_details;

Insert into Employee values(202,'Ramu');
Insert into Employee values(203,'Shamu');
select * from Employee;

Insert into Order_details_Branches values('B102',99,'07/29/2015','Raghu','B1102');
Insert into Order_details_Branches values('B103',101,'07/29/2015','Raghu','B1102');
select * from Order_details_Branches;

Insert into Members_Branches values(101,'B102');
Insert into Members_Branches values(102,'B103');
Insert into Members_Branches values(103,'B101');
select * from Members_Branches;

Insert into Branches_Employee values(202,'B102','Manager');
Insert into Branches_Employee values(203,'B103','Assistant_Manager');
select * from Branches_Employee;

select buyer_name from Branches natural join Order_details_Branches where no_of_products > 100 and Prod_name='P102' and date_of_purchase='08/20/2015';


 select Emp_name from Branches_Employee natural join Employee where Designation='Manager';
