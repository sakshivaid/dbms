﻿Question 2) 

create table dealer ( dealer_no varchar(20) primary key,
		     dealer_name varchar(20),
		     city  varchar (20),
		     order_id varchar(20),
		     date date		     
		     );
		  

create table fabric (   fabric_id varchar(10) primary key,
			fabric_type varchar(20),
			no_of_items numeric(10),
			unit_price numeric(5),
			check(fabric_type in( 'woven', 'knitten', 'manufactured'))
			); 
			

create table warehouse( ware_no varchar(10) primary key,
			ware_name varchar(20),
			incharge varchar(20),
			city varchar(10)
			);

create table fabric_order( order_id varchar(20) primary key,
		    payment numeric(10)
		    );	

/*create table placed( dealer_no varchar(20),
			date date,
			order_id varchar(20),
			primary key (dealer_no, order_id)
		);
		drop table placed;	*/

create table shipment( date_of_sh date,
			ware_no varchar(20),
			order_id varchar(20),
			primary key ( ware_no, order_id)
			);

create table contains (order_id varchar(20),
			fabric_id varchar(10) primary key
			);

insert into dealer values('abc12364', 'Naik', 'Hyderabad', 'fab87cv11','11-05-2015') ;
insert into dealer values('abc12365', 'Ramesh', 'Kanpur', 'fab87cv12','11-11-2015') ;
insert into dealer values('abc12366', 'Raju', 'Cochin', 'fab87cv13','12-09-2015') ;
insert into dealer values('abc12367', 'Sita ', 'Delhi', 'fab87cv14','04-06-2015') ;
insert into dealer values('abc12368', 'Geeta', 'Nagpur', 'fab87cv15','01-04-2015') ;
insert into dealer values('abc12369', 'Happy', 'Pune', 'fab87cv16','01-03-2015') ;

insert into fabric values('cot345', 'knitten', 35000, 350);
insert into fabric values('cot346', 'woven', 45000, 50);
insert into fabric values('cot347', 'knitten', 85000, 150);
insert into fabric values('cot348', 'manufactured', 31000, 250);
insert into fabric values('cot349', 'manufactured', 35060, 450);

insert into warehouse values('wh001', 'Rati', 'Komal', 'Hyderabad'); 
insert into warehouse values('wh002', 'Sati', 'Rajesh', 'Kanpur'); 
insert into warehouse values('wh003', 'R R', 'Aish', 'Delhi'); 
insert into warehouse values('wh004', 'S S', 'Rachana', 'Mumbai'); 
insert into warehouse values('wh005', 'M S', 'Suresh', 'Kollam'); 


select order_id from shipment natural join warehouse where city='Kollam';
select ware_name, sum(payment) from warehouse natural join shipment natural join fabric_order group by ware_name;
select dealer_name from dealer where city in( select city from warehouse);

select order_id from shipment natural join warehouse where ware_name = 'Kollam' 
and date_of_sh = ((select(extract(date from current_date)) - 2);

select dealer_name from dealer where dealer_name like '%Naik'
union
select dealer_name from dealer where dealer_name like 'Naik%';

create function new_place_order (fab_id varchar(10),fab_typ varchar(20), no_item numeric(10), dealer_id varchar(20), dealer_name varchar(20), city varchar(10)) 
returns integer as
$$
	Declare tot integer;
		price numeric(10);
		leftover numeric(10);

	Begin
		tot = (select no_of_items from fabric where fabric_id = fab_id);
		leftover = tot-no_item;
		 if tot >= no_item
			then 
				price = no_item * (select unit_price from fabric where fabric_id = fab_id);
				insert into fabric_order values( 'oder12',  price);
				update fabric set no_of_items = leftover;
				insert into dealer values(dealer_id, dealer_name, city, 'oder12', current_date);
				return 0;

		else
			raise notice 'no more fabric left';
			return -1;
		end if;
	end;
$$ language plpgsql;
select fabric_id from contains natural join order where order_id in ( select order_id from placed where dealer_name = 'Dayal' union select order_id from placed where dealer_name = 'Harsh')
                                                      


