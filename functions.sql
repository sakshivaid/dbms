﻿create table classroom ( roomno integer,
			capacity integer);

insert into classroom values ( 101, 50);
insert into classroom values ( 102, 60);
insert into classroom values ( 103, 60);
insert into classroom values ( 104, 50);
insert into classroom values ( 105, 100);
insert into classroom values ( 106, 50);

alter table classroom add constraint pkey primary key (roomno);
create table section ( cid varchar(6),
			secid varchar (6), 
			sem integer,
			 year integer,
			 primary key(cid,secid,sem,year));


insert into section values ('CSE340', 'CSEA', 5, 2015);
insert into section values ('ECE140', 'ECEB', 3, 2011);
insert into section values ('EEE111', 'EEEA', 6, 2011);
insert into section values ('CSE110', 'CSEB', 2, 2012);
insert into section values ('Ch210', 'IntMB', 1, 2013);

alter table section add roomno integer;
select * from section;

create function sec_update (varchar(6), varchar(6), integer, integer, integer) returns integer as
$$ 
	declare class_no integer;
		
	begin
	class_no = (select count(roomno) from section where sem not in (select sem from section 
						where year = $4 and roomno = $5) and year = $4 and roomno = $5);
		
	
	if class_no > 0
		then update section set roomno = $5 where cid = $1 and secid = $2 and sem = $3 and year = $4;
			return 0;
	else
		raise notice 'already alloted';
		return -1; 
	end if;
	end;
$$ language plpgsql;

create table takes ( student_id varchar(10),
			cid varchar(6),
			sem integer,
			year integer,
			secid varchar(6),
			primary key (student_id, cid, sem ,year, secid)
		);


create function register_student(varchar(6), varchar(6), integer, integer, varchar(10)) returns integer as
$$
	declare
	no_stud integer default 0;
	rlimit integer default 0;

	begin
	no_stud = (select count(*) from takes where cid = $1 and secid = $2 and sem = $3 and year = $4);
	rlimit = (select capacity from classroom natural join section where cid = $1 and secid = $2 and sem = $3 and year = $4);

	if no_stud < rlimit
		then insert into takes values ($5, $1, $3, $4, null);
			return 0;
	else
		raise notice ' limit reached';
		return -1;
	end if;
	end;
$$ language plpgsql;

select * from teaching;

create function ticket_book(tick_id varchar(10), flig_id varchar(10), durtn time, seatno varchar(5),
						tick_typ varchar(10), cust_id varchar(10)) returns integer as
$$
	declare
	no_pas integer default 0;
	plimit integer default 0;

	begin
	no_pas = (select count(*) from air_ticket where booking_status = 'confirmed' and flight_id = flig_id and
									ticket_type = tick_type and duration = durtn);
	plimit = (select no_of_seat from airplane where airplane_id = flig_id);

	
	if  no_pas < plimit
		then insert into air_ticket values (tick_id, cust_id, flig_id, durtn, seatno, tick_typ, 'confirmed', '12000',current_date, current_time);
			return 1;
	else
		insert into air_ticket values (tick_id, cust_id, flig_id, durtn, seatno, tick_typ, 'not-confirmed', '12000',current_date, current_time);
		return 0;
	end if;
	end;
$$ language plpgsql;

create or replace function cancel_tick (tick_id varchar(10)) returns integer as
$$
	declare canc date;

	begin 
	canc = (select dept_date from air_ticket natural join flight where ticket_id = tick_id);

	if (canc - current_date) > 2 
		 then delete from air_ticket where ticket-id = tick_id;
		return 1;

	else return -1;
end if;
end;
$$ language plpgsql;

create function add_it(year1 numeric, oore varchar(5), year2 numeric) returns numeric as
$$
     declare x numeric default 0;
     begin
	if oore = 'even'
	then x=0;
     else x=1;
     end if;

     insert into teaching(select emp_id, course_id, secid, semester, year2 from teaches where year = year1 and mod(semester,2) = x);
     return 0;
     end;
$$ language plpgsql;




