﻿create table passenger (
	passenger_no varchar(6), 
	name varchar(20),
	age numeric(3),
	address varchar(30),
	gender char(1),
	pincode numeric(8),
	state varchar(15),
	concession char(3),
	mobile_no varchar(10) 
);
alter table passenger add constraint passenger_pkey primary key(passenger_no, mobile_no);

create table train_master (
	train_no varchar(6),
	train_name varchar(15),
	train_src varchar(5),
	train_dest varchar(5),
	train_dept time,
	train_arrival time
);

alter table train_master add constraint train_master_pkey primary key(train_no);

create table station (
	station_code varchar(5),
	station_name varchar(20),
	metro char(1)
);

alter table station add constraint station_pkey primary key(station_code, metro);

insert into passenger values( '13467', 'sakshi', 19, 'ramanthapur hyderabad', 'f', 500013, 'telangana', 'stu', '8098716154');

insert into passenger values( '13437', 'sarika', 20, 'sarojininagar trissur', 'f', 500013, 'kerala', 'stu', '7098765123');

insert into passenger values( '38467', 'sreelakshmi', 18, 'ramnagar palakkad', 'f', 500013, 'kerala', 'stu', '9827654134');

insert into passenger values( '16762', 'nikhil', 19, 'kontapalli guntur', 'm', 500013, 'andhra', 'stu', '9208374651');

insert into passenger values( '68728', 'manideep', 20, 'sitapur palakkad', 'm', 500013, 'karnataka', 'stu', '6173290876');

insert into passenger values( '67289', 'rajat', 19, 'srikakulam vijaywada', 'm', 500013, 'haryana', 'stu', '8097865432');

insert into passenger values( '67819', 'pappu', 18, 'nirmla vihar palvel', 'm', 500013, 'mumbai', 'stu', '9876543210');

insert into passenger values( '09826', 'vinodh', 20, 'gontipuda vijaywada', 'm', 500013, 'west bengal', 'stu', '9988776655');

insert into passenger values( '66789', 'rene', 19, 'dilshukhnagar hyderbad', 'f', 500013, 'telangana', 'stu', '7862543190');

insert into passenger values( '53267', 'megha', 18, 'gitanagar trissur', 'f', 500013, 'kerala', 'stu', '7878787865');

insert into train_master values( '113245', 'konkani express', 'goa', 'tvm', '17:00', '1:00');

insert into train_master values( '100245', 'garibrath', 'hyd', 'ywt', '1:00', '5:30');

insert into train_master values( '113785', 'hyd express', 'hyd', 'tvm', '23:00', '1:45');

insert into train_master values( '123459', 'shtbd express', 'del', 'bpl', '12:00', '0:00');

insert into train_master values( '100009', 'duranto express', 'chn', 'viz', '17:00', '8:00');

insert into train_master values( '987650', 'kashi express', 'tvm', 'ksh', '1:00', '12:00');

insert into train_master values( '567487', 'delhi express', 'del', 'pkd', '12:00', '23:30');

insert into train_master values( '109876', 'mughal express', 'del', 'ksh', '6:00', '5:00');

insert into train_master values( '123098', 'rajput express', 'jpr', 'bpl', '2:00', '1:00');

insert into train_master values( '132167', 'cholas express', 'ywt', 'tvm', '18:00', '3:00');

insert into station values( 'mmr', 'manmad jn', 'y');

insert into station values( 'pune', 'pune jn', 'n');

insert into station values( 'ltt', 'lokmanyatilak', 'y');

insert into station values( 'koaa', 'kolkata', 'n');

insert into station values( 'anvt', 'anand vihar terminal', 'y');

insert into station values( 'dli', 'delhi', 'n');

insert into station values( 'apdj', 'alipur duar jn', 'y');

insert into station values( 'cstm', 'mumbai cst', 'y');

insert into station values( 'bsl', 'bhusaval', 'n');

insert into station values( 'scbd', 'secundrabad', 'y');

insert into station values('alp', 'allappey', 'n');

select * from station;

update station set station_name = 'allapuza' where station_name = 'allappey';

select * from passenger;

delete from passenger where mobile_no like '70%';

select * from train_master;

alter table train_master drop train_type;

alter table train_master add column train_type varchar(10) check (train_type in ('mail exp', 'passenger', 'superfast'));

update train_master set train_type = 'mail exp' where train_no = '113245';

update train_master set train_type = 'passenger' where train_no = '987650';

update train_master set train_type = 'superfast' where train_no = '100009';

update train_master set train_type = 'superfast' where train_no = '123459';

update train_master set train_type = 'passenger' where train_no = '100245';

update train_master set train_type = 'mail exp' where train_no = '113785';

update train_master set train_type = 'mail exp' where train_no = '567487';

update train_master set train_type = 'passenger' where train_no = '109876';

update train_master set train_type = 'mail exp' where train_no = '123098';

update train_master set train_type = 'superfast' where train_no = '132167';

insert into train_master values( '113700', 'pune express', 'pun', 'tvm', '20:00', '1:45', 'superfast');

insert into train_master values( '113798', 'MUMBAI express', 'ctm', 'tvm', '21:00', '2:45', 'passenger');

select train_name, train_no from train_master where lower(train_name) like'%pune%' or lower(train_name) like '%mumbai%'; 

select train_name as Train_Name, (train_src || '-'|| train_dest) as Source_to_Destination from train_master;

alter table train_master add foreign key (train_src) references station(station_code), add foreign key (train_dest) references station(station_code); 

insert into train_master values( '13111', 'bhul express', 'bsl', 'tvm', '11:00', '00:00', 'superfast');

create table train_fare (
		train_no varchar(6),
		from_station varchar(5),
		to_station   varchar(5),
		distance_km numeric(5),
		fare 	numeric(4),
		no_of_days integer,
		foreign key (train_no) references train_master,
		primary key (train_no, distance_km)
);


alter table train_fare add foreign key (from_station) references station(station_code), add foreign key (to_station) references station(station_code);


select count(train_no) from train_master where train_type = 'superfast';
	
select count(train_no), train_type from train_master group by train_type;

select train_name, train_no from train_master where train_type = 'passenger' order by train_no asc;

insert into train_fare values( '113245', 'mmr', 'ltt', 1600, 1650, 1);

select avg(fare) from train_fare where from_station = 'ltt' and to_station =  'mmr';

select from_station, to_station from train_fare having fare = max(fare) order by train_no;

select train_no from train_fare where no_of_days > 2;

alter table train_fare add commemcement date;

